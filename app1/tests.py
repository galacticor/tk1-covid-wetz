from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import *

# Create your tests here.
class TestGeneral(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get("/")
		self.assertEqual(response.status_code, 200)

	def test_event_post_is_exist(self):
		r= Client().post("/")
		self.assertEqual(r.status_code, 200)

	def test_event_post_cookies_is_worked(self):
		data = {
			'name' : "abc",
		}
		r= self.client.post("/", data)
		self.assertEqual(r.client.cookies.get('name').value, 'abc')