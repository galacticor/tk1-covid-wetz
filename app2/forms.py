from django import forms
from django.forms import ModelForm
from .models import *

class BlogForm(ModelForm):
    class Meta:
        model = Blog
        fields = ['title', 'content']

        widgets = {
            'title' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Your blog's title"}),
            'content' : forms.Textarea(attrs={'class': 'form-control', 'placeholder': "Your story..."}),
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['message']

        widgets = {
            'message' : forms.Textarea(attrs={'class': 'form-control', 'placeholder': "Your comment..."}),
        }