from django.db import models

# Create your models here.
class Visitor(models.Model):
    name = models.CharField(default="Anonymous", max_length=50, blank=True)

    def __str__(self):
        return self.name

class Comment(models.Model):
    message = models.CharField(max_length=500)
    commenter = models.ForeignKey(Visitor, related_name="visitor_comment", on_delete= models.SET_NULL, null=True, blank=True)
    date_created = models.DateField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.commenter, self.message)

class Blog(models.Model):
    title = models.CharField(max_length=100, unique=True)
    content = models.TextField()
    name = models.CharField(default="Anonymous", max_length=50, blank=True)
    likes = models.ManyToManyField(Visitor, related_name="blog_likes", blank=True)
    comment = models.ManyToManyField(Comment, blank=True)

    def __str__(self):
        return self.title

    def blogTotalLikes(self):
        return self.likes.all().count()