from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import *

# Create your tests here.
class TestIndex(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get("/blogs/")
		self.assertEqual(response.status_code, 200)

	def test_event_is_using_index_function(self):
		found = resolve("/blogs/")
		self.assertEqual(found.func, index)

	def test_event_is_using_template(self):
		response = Client().get("/blogs/")
		self.assertTemplateUsed(response, 'app2/blog.html')

class TestBlog(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get("/blogs/add-blog")
		self.assertEqual(response.status_code, 200)

	def test_event_is_using_blog_form_function(self):
		found = resolve("/blogs/add-blog")
		self.assertEqual(found.func, blog_form)

	def test_event_is_using_template(self):
		response = Client().get("/blogs/add-blog")
		self.assertTemplateUsed(response, 'app2/blog_form.html')

	def test_blog_is_not_exist_redirect(self):
		response = Client().get("/blogs/test/")
		self.assertEqual(response.status_code, 302)

	def test_blog_is_exist(self):
		b = Blog(title="test")
		b.save()
		response = Client().get("/blogs/test/")
		self.assertEqual(response.status_code, 200)

	def test_blog_is_using_blog_content_function(self):
		b = Blog(title="test")
		b.save()
		found = resolve("/blogs/test/")
		self.assertEqual(found.func, blog_content)

	def test_blog_is_using_template(self):
		b = Blog(title="test")
		b.save()
		response = Client().get("/blogs/test/")
		self.assertTemplateUsed(response, 'app2/blog_content.html')

	def test_event_url_post_is_exist(self):
		response = Client().post("/blogs/add-blog")
		self.assertEqual(response.status_code, 200)
			
	def test_form_blog(self):
		data = {
			'title':"test",
			'content':'ini content',
		}
		r = Client().post("/blogs/add-blog", data)
		test = Blog.objects.all()
		self.assertEqual(test.count(), 1)

	def test_form_blog_redirect_is_worked(self):
		data = {
			'title':"test",
			'content':'ini content',
		}
		r = Client().post("/blogs/add-blog", data)
		self.assertEqual(r.status_code, 302)

	def test_form_blog_redirect_is_exist(self):
		data = {
			'title':"test",
			'content':'ini content',
		}
		r = self.client.post("/blogs/add-blog", data, follow=True)
		self.assertRedirects(r, '/blogs/', status_code=302, 
        target_status_code=200, fetch_redirect_response=True)

	def test_visitor_model_is_exist(self):
		visitor = Visitor.objects.create(name="bambang")
		visitor.save()
		self.assertEqual(Visitor.objects.all().count(), 1)

	def test_comment_model_is_exist(self):
		visitor = Visitor.objects.create(name="bambang")
		comment = Comment(message="test", commenter=visitor)
		comment.save()
		self.assertEquals(Comment.objects.all().count(), 1)

	def test_blog_comment_model_is_exist(self):
		visitor = Visitor.objects.create(name="bambang")
		b = Blog(title="test")
		b.save()
		cmnt = b.comment.create(message="test", commenter=visitor)
		cmnt.save()
		self.assertEquals(b.comment.all().count(), 1)