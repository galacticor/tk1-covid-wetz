from django.urls import path
from . import views

app_name = 'blogs'

urlpatterns = [
    path('', views.index, name="index"),
    path('<str:title>/', views.blog_content, name="blog_content"),
    path('add-blog', views.blog_form, name="blog_form"),
    path('<str:title>/like/', views.blog_like, name="like"),
]