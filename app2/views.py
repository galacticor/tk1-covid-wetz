from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import *
# Create your views here.


def index(request):
	blogs = Blog.objects.all()
	return render(request, 'app2/blog.html', {'blogs': blogs})

def blog_form(request):
	form = BlogForm(request.POST or None)
	if request.method == "POST":
		if form.is_valid():
			dataBlog = form.cleaned_data
			if request.COOKIES.get('name') == None:
				newBlog = Blog(title=dataBlog['title'], content=dataBlog['content'], name="Anonymous")
			else:
				newBlog = Blog(title=dataBlog['title'], content=dataBlog['content'], name=request.COOKIES.get('name'))
			newBlog.save()
			return redirect('/blogs')

	return render(request, 'app2/blog_form.html', {"form":form, 'name': request.COOKIES.get('name')})

def blog_content(request, title):
	blogs = Blog.objects.all()

	if blogs.count() != 0: 
		blog = Blog.objects.get(title=title)
		form = BlogForm()
		comment_form = CommentForm()

		if request.method == "POST":
			comment_form = CommentForm(request.POST)
			if comment_form.is_valid():
				dataComment = comment_form.cleaned_data
				msg = dataComment['message']
				if request.COOKIES.get('name') == None:
					prsn = "Anonymous"
				else:
					prsn = request.COOKIES.get('name')
				blog.comment.create(message=msg, commenter=Visitor.objects.get(name=request.COOKIES.get('name')))
				blog.save()
				return redirect('/blogs/')
		
		context = {'blog': blog, "form": form, 'cform': comment_form}

		return render(request, 'app2/blog_content.html', context)

	else:
		return redirect('/blogs/')

# mekanisme likenya masih flaw, dengan problem berikut:
# setelah di-like, pas coba di refresh, tombol likenya ke reset (jadi kek belum di-like) dan counter like nya ke reset
# setelah di-like, pas pengen ngunjungin blog-nya yang udah di like, tombol likenya ke reset tapi counter like nya kaga ke reset

def blog_like(request, title):	
	blog = Blog.objects.get(title=title)
	form = BlogForm()
	person = request.COOKIES.get('name')
	comment_form = CommentForm()

	if person == None:
		visitor = Visitor(name="Anonymous")
		visitor.save()

	else:
		visitor, created = Visitor.objects.get_or_create(name=person)

		
	context={
		'visitor' : visitor,
		'liked' : False,
		'blog' : blog,
		'form' : form,
		'cform' : comment_form
	}

	if request.method == "POST":
		# setelah di like, rencananya form blog masih bisa dipake utk buat blog dan form comment masih bisa utk ngisi comment
		# gua coba terapin hal diatas dengan code "blog_content(request, title)", tapi kayaknya masih ngaco
		# karena setiap kali gw refresh setelah gua nambah comment, comment yang sama ketambah lagi wkwkw (apa emg kek gitu cara kerjanya wkwk)
		# mohon bantuannya
		blog_content(request, title) 
		if blog.likes.filter(name=visitor).exists():
			blog.likes.remove(visitor)
			context['liked'] = False
		else:
			blog.likes.add(visitor)
			context['liked'] = True
			
		blog.save()

	return render(request, 'app2/blog_content.html', context)