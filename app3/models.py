from django.db import models

# Create your models here.

class Feedback(models.Model):
    pesan = models.TextField(null=False, blank=False)
    nama = models.TextField(null=False, blank=False, default="Anonymous")

    def __str__(self):
        return self.pesan