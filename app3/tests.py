from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import *
  
# Create your tests here.
class App3Test(TestCase):
    def test_about_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)
    
    def test_about_using_about_us_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'app3/about_us.html')
    
    def test_about_using_index_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, index)
    
    def test_about_text_in_html(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn("US.", html_response)
        self.assertIn("here\'s our <span>team</span>,<br>desparetely need more <span>sleep", html_response)
        self.assertIn("EBA", html_response)
        self.assertIn("Eba Ginting", html_response)
        self.assertIn("\"it's just three letters. no mispell is allowed.\"", html_response)
        self.assertIn("TIMMY", html_response)
        self.assertIn("Timothy Efraim", html_response)
        self.assertIn("\"heccing hecc.\"", html_response)
        self.assertIn("Widy", html_response)
        self.assertIn("Widyanto Hadi", html_response)
        self.assertIn("\"Jangan kebanyakan belajar\"", html_response)
        self.assertIn("Zul", html_response)
        self.assertIn("Zulfahri Haradi", html_response)
        self.assertIn("\"Roses are red, violets are blue, ppw is pain\"", html_response)
        self.assertIn("Got a <span>feedback</span> for us? Let us <span>hear!", html_response)

class App3ModelTest(TestCase):
    def test_feedback_model_exist(self):
        Feedback.objects.create(pesan="test")
        self.assertEqual(Feedback.objects.all().count(), 1)

    def test_can_add_feedback(self):
        Client().post('/about/', {'pesan':'pesan masuk'})
        self.assertEqual(Feedback.objects.all().count(), 1)

    def test_cannot_add_empty_feedback(self):
        Client().post('/about/', {})
        self.assertNotEqual(Feedback.objects.all().count(), 1)

class App3FeedbackTest(TestCase):    
    def test_feedback_part_exist(self):
        response = Client().post('/about/', {'pesan':'pesan masuk'})
        html_response = response.content.decode('utf8')
        self.assertIn("Thanks for your <span>feedback!</span>", html_response)
    
    def test_can_add_and_show_feedback(self):
        response = Client().post('/about/', {'pesan':'to be found'})
        html_response = response.content.decode('utf8')
        self.assertIn("to be found", html_response)
    