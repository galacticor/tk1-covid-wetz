from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Feedback
from .forms import FeedbackForm
# Create your views here.


def index(request):
	form = FeedbackForm()
	context = {
		'form' : form,
		'submitted' : False}

	if request.method == "POST":
		form = FeedbackForm(request.POST)
		if form.is_valid():
			inputted_pesan = form.cleaned_data
			if request.COOKIES.get('name') == None:
				new_feed = Feedback(pesan=inputted_pesan['pesan'], nama="Anonymous")
			else:
				new_feed = Feedback(pesan=inputted_pesan['pesan'], nama=request.COOKIES.get('name'))
			new_feed.save()
			feedbacks = Feedback.objects.all()
			context['feedbacks'] = feedbacks
			context['submitted'] = True
			return render(request, 'app3/about_us.html', context)
		else:
			form = FeedbackForm()
	return render(request, 'app3/about_us.html', context)
