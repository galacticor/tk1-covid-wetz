from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Games)
admin.site.register(Music)
admin.site.register(Movies)
admin.site.register(Podcast)