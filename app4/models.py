from django.db import models

# Create your models here.

class Games(models.Model):
	name = models.CharField(max_length=120, unique=True)
	desc = models.TextField(blank=True, null=True)
	poll = models.PositiveIntegerField(default=0)
	avg = models.PositiveIntegerField(default=0)

	def __str__(self):
		return "{} - {}".format(self.id, self.name)

	def Poll(self):
		self.poll += 1
		self.save()


class Music(models.Model):
	name = models.CharField(max_length=120, unique=True)
	desc = models.TextField(blank=True, null=True)
	poll = models.PositiveIntegerField(default=0)
	avg = models.PositiveIntegerField(default=0)

	def __str__(self):
		return "{} - {}".format(self.id, self.name)

	def Poll(self):
		self.poll += 1
		self.save()


class Movies(models.Model):
	name = models.CharField(max_length=120, unique=True)
	desc = models.TextField(blank=True, null=True)
	poll = models.PositiveIntegerField(default=0)
	avg = models.PositiveIntegerField(default=0)

	def __str__(self):
		return "{} - {}".format(self.id, self.name)

	def Poll(self):
		self.poll += 1
		self.save()


class Podcast(models.Model):
	name = models.CharField(max_length=120, unique=True)
	desc = models.TextField(blank=True, null=True)
	poll = models.PositiveIntegerField(default=0)
	avg = models.PositiveIntegerField(default=0)

	def __str__(self):
		return "{} - {}".format(self.id, self.name)

	def Poll(self):
		self.poll += 1
		self.save()
