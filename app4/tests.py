from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import *

# Create your tests here.
class TestGeneral(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get("/recommendation/")
		self.assertEqual(response.status_code, 200)


class TestGames(TestCase):
	def setUp(self):
		a = Games(name="abc")
		a.save()

	def testPoll(self):
		a = Games.objects.get(id=1)
		a.Poll()
		self.assertEqual(a.poll, 1)

	def test_printModel(self):
		a = Games.objects.get(id=1)
		self.assertEqual(str(a), "1 - abc")

	def test_event_url_is_exist(self):
		a = Games(name="a")
		a.save()
		response = Client().get("/recommendation/games/")
		self.assertEqual(response.status_code, 200)

	def test_poll_url_is_exist(self):
		data = {
			'item': 1,
		}
		response = Client().post('/recommendation/games/poll/', data)
		self.assertEqual(response.status_code, 302)

	def test_poll_url_redirect_is_exist(self):
		data = {
			'item': 1,
		}
		response = self.client.post('/recommendation/games/poll/', data, follow = True)
		self.assertRedirects(response, '/recommendation/games/', status_code=302, 
        target_status_code=200, fetch_redirect_response=True)

class TestMovies(TestCase):
	def setUp(self):
		a = Movies(name="abc")
		a.save()

	def testPoll(self):
		a = Movies.objects.get(id=1)
		a.Poll()
		self.assertEqual(a.poll, 1)

	def test_printModel(self):
		a = Movies.objects.get(id=1)
		self.assertEqual(str(a), "1 - abc")

	def test_event_url_is_exist(self):
		response = Client().get("/recommendation/movies/")
		self.assertEqual(response.status_code, 200)

	def test_poll_url_is_exist(self):
		data = {
			'item': 1,
		}
		response = Client().post('/recommendation/movies/poll/', data)
		self.assertEqual(response.status_code, 302)

	def test_poll_url_redirect_is_exist(self):
		data = {
			'item': 1,
		}
		response = self.client.post('/recommendation/movies/poll/', data, follow = True)
		self.assertRedirects(response, '/recommendation/movies/', status_code=302, 
        target_status_code=200, fetch_redirect_response=True)

class TestMusic(TestCase):
	def setUp(self):
		a = Music(name="abc")
		a.save()

	def testPoll(self):
		a = Music.objects.get(id=1)
		a.Poll()
		self.assertEqual(a.poll, 1)

	def test_printModel(self):
		a = Music.objects.get(id=1)
		self.assertEqual(str(a), "1 - abc")

	def test_event_url_is_exist(self):
		response = Client().get("/recommendation/music/")
		self.assertEqual(response.status_code, 200)

	def test_poll_url_is_exist(self):
		data = {
			'item': 1,
		}
		response = Client().post('/recommendation/music/poll/', data)
		self.assertEqual(response.status_code, 302)

	def test_poll_url_redirect_is_exist(self):
		data = {
			'item': 1,
		}
		response = self.client.post('/recommendation/music/poll/', data, follow = True)
		self.assertRedirects(response, '/recommendation/music/', status_code=302, 
        target_status_code=200, fetch_redirect_response=True)

class TestPodcast(TestCase):
	def setUp(self):
		a = Podcast(name="abc")
		a.save()

	def testPoll(self):
		a = Podcast.objects.get(id=1)
		a.Poll()
		self.assertEqual(a.poll, 1)

	def test_printModel(self):
		a = Podcast.objects.get(id=1)
		self.assertEqual(str(a), "1 - abc")

	def test_event_url_is_exist(self):
		response = Client().get("/recommendation/podcast/")
		self.assertEqual(response.status_code, 200)

	def test_poll_url_is_exist(self):
		data = {
			'item': 1,
		}
		response = Client().post('/recommendation/podcast/poll/', data)
		self.assertEqual(response.status_code, 302)

	def test_poll_url_redirect_is_exist(self):
		data = {
			'item': 1,
		}
		response = self.client.post('/recommendation/podcast/poll/', data, follow = True)
		self.assertRedirects(response, '/recommendation/podcast/', status_code=302, 
        target_status_code=200, fetch_redirect_response=True)