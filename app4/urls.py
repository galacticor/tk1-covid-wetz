from django.urls import path
from . import views

app_name = 'recommendation'

urlpatterns = [
    path('', views.index, name="index"),
    path('games/', views.games, name="games"),
    path('movies/', views.movies, name="movies"),
    path('music/', views.music, name="music"),
    path('podcast/', views.podcast, name="podcast"),
    path('<str:model>/poll/', views.poll, name="poll"),
]
