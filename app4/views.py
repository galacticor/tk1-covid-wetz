from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
# Create your views here.


def index(request):
	return render(request, "app4/rekomendasi.html")

# def model(request, model):
# 	objects = 0
# 	templates = ''
# 	if model == "Games":
# 		objects = Games.objects.all()
# 		templates = 'app/games.html'
# 	elif model == "Movies":
# 		objects = Movies.objects.all()
# 		templates = 'app/movies.html'
# 	elif model == "Music":
# 		objects = Music.objects.all()
# 		templates = 'app/musics.html'
# 	elif model == "Podcast":
# 		objects = Podcast.objects.all()
# 		templates = 'app/podcast.html'

# 	total = 0
# 	for o in objects:
# 		total += o.poll

# 	for o in objects:
# 		o.avg = round(o.poll / total * 100)
# 		o.save()

# 	data={
# 		'object':games,
# 		'model' : 'games',
# 		'cek' : request.COOKIES.get('poll_games'),
# 		'name': request.COOKIES.get('name'),

def games(request):
	games = Games.objects.all().order_by('id')
	total = 0
	for g in games:
		total += g.poll

	for g in games:
		try:
			g.avg = round(g.poll / total * 100)
		except:
			g.avg = 0
		g.save()

	data={
		'object':games,
		'model' : 'games',
		'cek' : request.COOKIES.get('poll_games'),
		'name': request.COOKIES.get('name'),
	}

	return render(request, "app4/games.html", data)

def music(request):
	music = Music.objects.all().order_by('id')
	total = 0
	for m in music:
		total += m.poll

	for m in music:
		try:
			m.avg = round(m.poll / total * 100)
		except:
			m.avg = 0
		m.save()


	data={
		'object':music,
		'model' : 'music',
		'cek' : request.COOKIES.get('poll_music'),
		'name': request.COOKIES.get('name'),
	}

	return render(request, "app4/musics.html", data)

def movies(request):
	movies = Movies.objects.all().order_by('id')
	total = 0
	for m in movies:
		total += m.poll

	for m in movies:
		try:
			m.avg = round(m.poll / total * 100)
		except:
			m.avg = 0
		m.save()


	data={
		'object':movies,
		'model' : 'movies',
		'cek' : request.COOKIES.get('poll_movies'),
		'name': request.COOKIES.get('name'),
	}

	return render(request, "app4/movies.html", data)	

def podcast(request):
	podcast = Podcast.objects.all().order_by('id')
	total = 0
	print(podcast)
	for p in podcast:
		total += p.poll

	for p in podcast:
		try:
			p.avg = round(p.poll / total * 100)
		except:
			p.avg = 0
		p.save()

	data={
		'object':podcast,
		'model' : 'podcast',
		'cek' : request.COOKIES.get('poll_podcast'),
		'name': request.COOKIES.get('name'),
	}

	return render(request, "app4/podcast.html", data)

def poll(request, model):
	if model == 'games':
		id = request.POST.get('item')
		game = Games.objects.get(id=id)
		game.Poll()
		r = redirect("/recommendation/games/")
		r.set_cookie('poll_games', True)
		return r

	if model == 'movies':
		id = request.POST.get('item')
		movie = Movies.objects.get(id=id)
		movie.Poll()
		r = redirect("/recommendation/movies/")
		r.set_cookie('poll_movies', True)
		return r

	if model == 'music':
		id = request.POST.get('item')
		music = Music.objects.get(id=id)
		music.Poll()
		r = redirect("/recommendation/music/")
		r.set_cookie('poll_music', True)
		return r

	if model == 'podcast':
		id = request.POST.get('item')
		podcast = Podcast.objects.get(id=id)
		podcast.Poll()
		r = redirect("/recommendation/podcast/")
		r.set_cookie('poll_podcast', True)
		return r